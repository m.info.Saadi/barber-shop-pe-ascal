import { shallowMount } from "@vue/test-utils"
import Header from "@/components/Header.vue"

test("Comprobar el componente Header", () => {
  //crear componente con las propiedades

  const wrapper = shallowMount(Header);

  expect(wrapper.emitted().search).toBe(undefined);
  //buscamos la clase de la plantilla html o el componente  con (findAll)

  const items = wrapper.findAll(".header").wrappers;

  wrapper.findAll(".header").wrappers;

  //para acceder dentro del wrapper usamos .vm
  expect(items.length).toBe(1);
});

/* test.skep("emite evento click correspondiente", async () => {
  //monto el componente
  const wrapper = shallowMount(Header);
  // comprueba que no se han emitido eventos
  expect(wrapper.emitted().administrator).toBe(undefined);

  const btnBackHome = wrapper.find(".btn-home");
  btnBackHome.trigger("click");

  await wrapper.vm.$nextTick();
  // compruebo que se han emitido los eventos que me interesan
  expect(wrapper.emitted().administrator.length).toBe(1);
  expect(wrapper.emitted().administrator[0]).toEqual([]);
});
 */